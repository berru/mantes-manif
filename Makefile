.PHONY: test deploy

index.html: index.md
	pandoc -s index.md -o index.html --standalone --include-in-header style.html --metadata title="Mantes-Manif" --include-after-body footer.html

deploy: index.html 
	mkdir -p public/
	mv index.html public/
	cp *.html public/
	cp *.jpeg public/
	cp *.jpg public/
	cp *.js public/
	cp *.pdf public/
	cp *.ttf public/

test: deploy
	qutebrowser public/index.html

publish:
	git add -A; git commit && git push gitlab ni_dieu_ni_maitre
