const defaultTitle = "Bullshit bingo";

const bullshits = [
  "Disruptif",
  "Projet",
  "Scalable",
  "Compétitivité",
  "Contre-verité",
  "Mouvement social",
  "Rigueur",
  "Vidéoprotection",
  "Force de l'ordre",
  "Collaborateur/Collaboratrice",
  "Startup",
  "Licorne",
  "Digitaliser",
  "Pitcher",
  "Workforce",
  "ASAP",
  "Challenge",
  "Gamifier",
  "Islamo-gauchiste",
  "Woke",
  "Antirépublicain",
  "Éco-terroriste",
  "Valeur Travail",
  "Valeur Famille",
  "Valeur Patrie"
];

const rating = [
  "Jusqu'ici tout va bien",
  "C'est un peu con",
  "Nan mais sérieux",
  "On touche le fond",
];

// Triggered on page load
function onLoad() {
  generateQuizz();
  var idList = loadIdsFromUrl();
  updateTitle(idList);
  setInputChecked(idList);
  saveInUrl(idList);
}

//Checks the inputs in the document
function setInputChecked(idList) {
  for (const input of document.querySelectorAll("input[type='checkbox']")) {
    input.checked = idList.includes(input.id);
  }
}

// Generates all the clickable items
function generateQuizz() {
  var grid = document.createElement("div");
  grid.setAttribute("class", "grid");
  for (var i = 0; i < bullshits.length; i++) {
    grid.appendChild(createClickableDiv(i, bullshits[i]));
  }
  document.body.appendChild(grid);
}

//Update h1 and title elements
function updateTitle(checkedIds) {
  var titleText = defaultTitle;

  if (checkedIds.length !== 0) {
    var titleText = generateTitle(checkedIds);
  }

  document.getElementsByTagName("h1")[0].innerText = titleText;
  document.title = titleText;
}

// Triggered when an item is clicked
function onCheckboxChanged(event) {
  var checked = loadIdsFromDocument();
  updateTitle(checked);
  saveInUrl(checked);
}

// Saves the checked items in the URL
function saveInUrl(checkedInputIdList) {
  var root = String(window.location).split("#")[0];
  var ids = checkedInputIdList.join("-");
  var url = `${root}#${ids}`;
  window.location = url;
  document.getElementById("share").value = url;
}

//Returns the list of input checked from the DOM.
function loadIdsFromDocument() {
  return Object.values(document.querySelectorAll("input:checked")).map(
    (input) => input.id
  );
}

//Generate the title string from the checked id list
function generateTitle(idList) {
  var score = idList.length / bullshits.length;
  var ratingIndex = Math.round(score * (rating.length - 1));
  return `Score: ${idList.length}/${bullshits.length} ${rating[ratingIndex]}`;
}

// Loads the checked boxes from the URL (called once on loading)
function loadIdsFromUrl() {
  var regex = /#[0-9]+(-[0-9]+)*/gm;
  var url = String(window.location);
  if (regex.exec(url) === null) return [];
  else return url.split("#")[1].split("-");
}

// Creates a single clickable item
// i.e. a <div> containing the <input> and <label> elements
function createClickableDiv(id, content) {
  var container = document.createElement("div");
  var input = document.createElement("input");
  var label = document.createElement("label");

  container.setAttribute("class", "item");

  input.setAttribute("type", "checkbox");
  input.setAttribute("onchange", "onCheckboxChanged(event)");
  input.id = id;

  label.setAttribute("for", id);
  label.textContent = content;

  // document.body.appendChild(createClickableDiv(i, bullshits[i]));
  container.appendChild(input);
  container.appendChild(label);
  return container;
}
