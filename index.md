## l'agenda des luttes du Mantois

<!-- [la Mairie](https://goo.gl/maps/xK6PFaFAEg1xXpCM8) -->
### Évènements à venir


<!-- - 3 décembre: 14h00, [Rassemblement](./gaza.jpeg) contre le massacre a Gaza à la [Dalle du valle fourré](https://maps.app.goo.gl/zEL5LS347XkDbqp38)  -->
- 2 Février : 19h00, [Soirée ludique](./antifa.jpg) festive et antifasciste à la librairie [la nouvelle réserve](https://maps.app.goo.gl/YcBWD28F1iMJqAx36). Repas avec les apports de chacun autour du jeu "antifa". Inscriptions auprès de contact@solidaires78.org


### Liens utiles:

<!-- - Pour soutenir localement les camarades en grève : [caisse de grève interprofessionnelle du mantois](https://sudeducation78.ouvaton.org/Une-caisse-de-greve-interpro-dans-le-Mantois) -->
- [Pétition pour la dissolution de la BRAV-M](https://petitions.assemblee-nationale.fr/initiatives/i-1446)
- En cas de répression, contacter [le RAJ : 07 52 95 71 11](https://rajcollective.noblogs.org/les-collectifs-locaux/paris/)

### Revue de presse:

- [Mantes: SOS Racisme appelle à une marche contre les violences policières](https://actu.fr/ile-de-france/mantes-la-jolie_78361/mantes-la-jolie-sos-racisme-appelle-a-une-marche-contre-les-violences-policieres_59832765.html)

